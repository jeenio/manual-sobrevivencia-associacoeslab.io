module.exports = {
  title: 'Manual de Sobrevivencia das Associações em Portugal',
  description: 'Manual de apoio a logistica e parte juridica das Associações em Portugal',
  base: process.env.VUEPRESS_BASE || '/',
  dest: 'public',
  head: [
    ['link', { rel: 'apple-touch-icon', sizes: '180x180', href: '/icons/apple-touch-icon.png' }],
    ['link', { rel: 'icon', type: 'image/png', sizes: "16x16", href: '/icons/favicon-16x16.png' }],
    ['link', { rel: 'icon', type: 'image/png', sizes: "32x32", href: '/icons/favicon-32x32.png' }],
    ['link', { rel: 'manifest', href: '/manifest.json' }],
    ['link', { rel: 'mask-icon', color: '#5bbad5', href: '/icons/safari-pinned-tab.svg' }],
    ['meta', { name: "msapplication-config", content: "/browserconfig.xml"}],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
    ['meta', { name: 'msapplication-TileColor', content: '#603cba' }],
    ['meta', { name: 'msapplication-TileImage', content: '/icons/mstile-144x144.png' }],
    ['meta', { name: 'theme-color', content: '#fffff0' }]
  ],
  themeConfig: {
      repo: 'https://gitlab.com/manual-sobrevivencia-associacoes/manual-sobrevivencia-associacoes.gitlab.io',
    editLinks: true,
    docsDir: 'docs',
    smoothScroll: true,
    editLinkText: 'Ajude-nos a melhorar esta página!',
    lastUpdated: 'Informção atualizada em',
    sidebar: {
      // https://github.com/vuejs/vuepress/issues/984
      sidebarDepth: 3,
      '/guide/': getGuideSidebar('Criar a Associação', 'Gerir a Associação', 'Apêndices'),
    },
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Manual', link: '/guide/' },
      { text: 'Contribuir', link: '/guide/contribuir' }
    ]
  },
  plugins: {
      '@vuepress/back-to-top': true,
      'vuepress-plugin-export': true,
      'vuepress-plugin-zooming': true,
      'vuepress-plugin-one-click-copy': {
        copySelector: ['div[class*="language-"] pre', 'div[class*="aside-code"] aside'],
        copyMessage: 'Copied!',
        duration: 600,
        showInMobile: false
      },
      'check-md': true
  }
}

function getGuideSidebar (groupA, groupB, groupC) {
  return [
    {
      title: groupA,
      collapsable: false,
      children: [
        '',
        'antes-criar-associacao',
        'criar-associacao',
        'apos-criar-associacao'  
      ]
    },
    {
      title: groupB,
      collapsable: false,
      children: [
        'contabilidade',
        'responsabilidades',  
        'assembleias-gerais',
	'outras-burocracias'
      ]
    },
    {
      title: groupC,
      collapsable: false,
      children: [	    
        'contribuir',
        'glossario',
        'livro-de-atas',
        'ajuda-portal-financas'
      ]
    }
  ]
}
