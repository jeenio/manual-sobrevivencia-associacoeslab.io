# Contabilidade

::: warning AVISO

Este manual ainda está em desenvolvimento e como tal, não recomendamos
ainda o seu uso. Caso deseje ajudar, veja como
[contribuir](contribuir.md).

:::

As associações podem optar por dois métodos de contabilidade:

  - **[contabilidade organizada](#contabilidade-organizada)** - para
    quando movimenta > 30.000 € / ano aprox. ou tem algum funcionario,
    neste caso, a associação contrata um(a) contabilista para tratar
    do trabalho.

  - **[contabilidade simplificada](#contabilidade-simplificada)** -
    regime mais simples de contabilidade. É a própria associação a
    tratar das suas contas.

## To Read
* [FAQ Associação na hora](www.associacaonahora.mj.pt/seccoes/ANH-Faqs.pdf#page11) - artigos 19, 21
* [Código do Imposto sobre o Rendimento das Pessoas Coletivas (IRC)](https://dre.pt/web/guest/legislacao-consolidada/-/lc/64205634/view?q=DECRETO-LEI+N.%C2%BA%20442-B%2F88).

## Tesoureiro
É a pessoa responsável pelas transações financeiras da associação. 

Responsabilidades:
  - fazer a gestão das quotas
  - pagar as contas
  - examinar gastos
  - manter em dia os livros de contas
  - manter o blanço de conta (pq os contabilistas não fazem isto por nós)
  - dar informações sobre a posição financeira da organização
  - preparar o relatório e contas a apresentar à Assembleia Geral Ordinária
  - aconselhar quanto ao uso de fundos para fins especiais e sobre as finanças da organização em geral.

> O tesoureiro deve apresentar a situação financeira da organização
periodica- mente. Se o fizer de forma regular, facilita a preparação
do relatório de con- tas anual a apresentar à Assembleia Geral
Ordinária.  Ao preparar um relatório de receitas e despesas, o
tesoureiro deve verificar primeiro todas as receitas pelo livro
correspondente e pelos talões de conta bancária, ou os recibos no caso
de pagamentos feitos em dinheiro. Deve verificar se os documentos
estão todos correctamente numerados, etc. Se a contabilidade for
rigorosa, a preparação de um relatório simples não será difícil.

Dicas:
- o tesoureiro deve ir fazendo as contas progressivamente e não deixar
  acumular trabalho
- deve adicionar comentários a cada transação a fim de não se esquecer
  e para justificar caso haja inspeção das finanças.


Contabilidade Simplificada
--------------------------

Modelo mais simples de contabilidade. Não requer nomear [Técnico
Official de Contas (TOC)](glossario.md#tecnico-oficial-de-contas), nem
utilizar software de contabilidade certificado. É necessário, contudo
passar recibos quando requerido por alguém. (TODO encontrar fonte).

::: details sobre a declaração trimestral de IVA

> A associação encontra-se isenta de IVA portanto pelo menos com
> contabilidade simplificada não parece ser preciso submeter
> trimestralmente a declaração de que a associação paga 0 € de IVA no
> portal das finanças pois a declaração de início de atividade é uma
> prova disso.
>
>  -- fonte: questão colocada num balcão finanças

:::

### Prestação de serviços alinhados com missão

Imagine-se que a associação é convidada para fazer um serviço que vai
ao encontro da sua missão (tal como definida nos estatos) e cujo valor
reverterá na totalidade para a própria.

Então é possível cobrar um valor monetário por essa ação com insenção
de iva nas condições:

> 19) As prestações de serviços e as transmissões de bens com elas
> conexas efectuadas no interesse colectivo dos seus associados por
> organismos sem finalidade lucrativa, desde que esses organismos
> prossigam objectivos de natureza política, sindical, religiosa,
> humanitária, filantrópica, recreativa, desportiva, cultural, cívica ou
> de representação de interesses económicos e a única contraprestação
> seja uma quota fixada nos termos dos estatutos;
>
> 20) As transmissões de bens e as prestações de serviços efectuadas
> por entidades cujas actividades habituais se encontram isentas nos
> termos dos n.os 2), 6), 7), 8), 9), 10), 12), 13), 14) e 19) deste
> artigo, aquando de manifestações ocasionais destinadas à angariação de
> fundos em seu proveito exclusivo, desde que esta isenção não provoque
> distorções de concorrência;
>                   -- [artigo 9º. do Código do IVA][codigo-IVA-9]


::: details requisito extra por confirmar

Adicionalmente para ser isento de IVA tem de ter um valor menor que
7500€. [procurar fonte no código do IVA]

:::

[codigo-IVA-9]: https://dre.pt/web/guest/legislacao-consolidada/-/lc/125734479/202003051859/73779587/diploma/indice

Claro que depois é necessário declara essa atividade no [Modelo
22](#modelo-22).

### Livros

  - [Livro de caixa](#livro-de-caixa)
  - [Livro de bancos](#livro-de-bancos)
  - ficheiro de correspondência entre os dois

#### Livro de caixa
> No livro de caixa é registado tudo o que é pago ou recebido por
caixa. A informática facili- ta bastante este procedimento porque em
excel pode ser criada uma folha para cada mês do ano, onde se faz o
registo das entradas e das saídas.  Estes registos devem ser sem- pre
acompanhados por um recibo ou factura/recibo que comprova a
despesa. Todos os documentos devem ser guardados em pastas devidamente
organizados por meses, não devem apenas ser registados.  Há também
quem acompanhe os documentos (facturas/recibos, recibo, ou outros) com
uma folha de pagamento/recebimento onde identifica e caracteriza a
operação.  O livro de caixa deve ser mantido em dia, a fim de permitir
conhecer em qualquer momen- to a posição financeira da organização
>
> -- *[Guia Prático para Associações sem Fins Lucrativos](https://www.cm-seixal.pt/sites/default/files/documents/02_13_06_documentos_guiapratico.pdf)*

#### Livro de bancos
> Existem livros de bancos, tal como os de caixa, à venda já preparados para o efeito,
no entanto também podem ser criados em ficheiros de excel, construindo um para
cada conta com várias folhas consoante o mês do ano.  
Este registo deve sempre ser acompanhado por documento justificativo. No caso de
ser efectuado um depósito (entrada de dinheiro para a associação), toda a infor-
mação referente ao mesmo deverá ser anexada na pasta adequada, o talão de depósito
 com o número correspondente ao da folha de banco, a cópia de cheque
(caso tenha sido pagamento por cheque) e a cópia do documento emitido pela asso-
ciação (recibo ou outro documento comprovativo do recebimento).

> Se se tratar de um levantamento (saída de dinheiro da conta da associação) a infor-
mação resultante do  mesmo deverá ser anexada com o número correspondente ao
da folha de banco, emitida uma nota de despesa  e documento comprovativo da
despesa (factura e respectivo recibo ou documento equivalente).
Quando se trata de uma transferência bancária, o banco envia um talão a avisar
que entrou ou que saiu dinheiro conforme se trate de um recebimento ou de um
pagamento. O talão de aviso do banco deve ser arquivado na respectiva pasta e
posteriormente registado na folha do banco como entrada ou saída. O número que
lhe foi atribuído na folha do banco deve constar no documento. Em anexo deve ser
colocada uma folha de pagamentos/recebimentos  interna da associação a identi-
ficar a operação.

> De vez em quando, a caixa deve ser conferida para que se verifique se o valor
da sua folha corresponde ao valor físico existente. Para se obter o valor dos
bancos, basta somar os saldos das várias contas pelos extractos e compará-
-los com os das folhas do banco. Por vezes, podem existir diferenças que
resultem, por exemplo, de despesas que o banco cobra para realizar algumas
operações. Pode também acontecer que algum cheque que já foi entregue
para  pagamento  ainda  não  tenha  sido  descontado.  Portanto,  há  que  ter
atenção e periodicamente fazer uma comparação entre o extracto bancário e
a folha de banco correspondente. 

> -- *[Guia Prático para Associações sem Fins Lucrativos](https://www.cm-seixal.pt/sites/default/files/documents/02_13_06_documentos_guiapratico.pdf)*

#### Ficheiro de Correspondência
Ficheiro de correspondência entre livro de bancos e livro de caixa.

### Calculo do Balanço
> O balanço é obtido deduzindo as despesas das receitas; a quantia
resultante deve ser comparada com o dinheiro disponível na conta
bancária e com as quantias existentes em dinheiro, de forma o mais
simplista possível.  Depois de analisado e aprovado em Assembleia
Geral, o relatório de contas será acrescentado ao relatório anual de
actividades, juntamente com a lista de quem o assina.

### Emissão de faturas

Em regime simplificado não é necessária a emissão de faturas com
software certificado. Basta dar um comprovativo do pagamento quando a
pessoa que fez uma transação para a associação o pede.

::: details EXEMPLO (passar recibos) (TODO melhorar exemplo e comparar)

A empresa X faz uma doação de Y € para a associação Z. Então a
associação Z produz uma carta com as seguintes indicações e que depois
envia para a morada de quem doou:

- nome da organização
- texto a dizer q a empresa X deu Y € (descrever donativos e dar a
  informação toda possível)
- data
- assinatura da(o) representante
- carimbo

:::


### Documentos a entregar

| Descrição                               | Prazo / Periodicidade        | Documento a preencher         | Fonte                          |
|-----------------------------------------|------------------------------|-------------------------------|--------------------------------|
| **Declaração periódica de rendimentos** | Anualmente até final de Maio | [Modelo 22 do IRC][modelo-22] | [Artigo 120.º do IRC][120-IRC] |
|                                         |                              |                               |                                |

#### Modelo 22

[modelo-22]: https://www.portaldasfinancas.gov.pt/pt/pf/html/entregaIRC.html
[120-IRC]: https://dre.pt/web/guest/legislacao-consolidada/-/lc/117352449/201902031228/73652933/diploma/indice

Contabilidade Organizada
------------------------

No caso da organização ter contabilidade organizada, esta **deve ser
da responsabilidade de um especialista** que pode ou não per- tencer à
associação

> Quando a associação dispõe de contabilidade organizada,
> **periodicamente deve enviar os documentos devidamente organizados
> numa pasta para o TOC (Técnico Oficial de Contas)** classificar e
> apurar os impostos.
>
> Será prudente garantir que qualquer gasto elevado ou não habitual
> seja convenientemente autorizado e registado em acta, para deste
> modo precaver problemas futuros.
>
> -- *[Guia Prático para Associações sem Fins Lucrativos](https://www.cm-seixal.pt/sites/default/files/documents/02_13_06_documentos_guiapratico.pdf)*


### Pagar salários
------------------

Quando a associação contrata alguém (pessoa administrativa, em
particular) tem que receber vencimento declarado (obviamente) deverá
ser obrigatório fazer **seguro de trabalho** e efetuar **descontos
para a segurança social**.

Convém ser uma empresa de contabilidade a fazer o processamento de
salários, mas quem faz o pagamento são os sócios que [a associação
obriga](glossario.md#a-associação-obriga-se-a-X) (administração).


### Emissão de faturas

Toda e qualquer saida de dinheiro tem de ser registada numa fatura,
mas uma só pode agregar várias saidas de dinheiro (ex: vendas durante
um vento somam 300€ então depois imitimos uma fatura para cliente
indiferenciado e guardamos na pasta da contabilidade)

### Documentos a entregar

A lista de documentos que devemos estar atentos para entregar atempadamente.

TODO: Ler mais http://contabilistas.info/index.php?topic=29606.0

- **Declaração de inscrição, alterações ou cessação** - artigos 118.º
  e 119.º do CIRC
- **Declaração periódica de rendimentos** - artigos 120.º do CIRC
- **Declaração anual de informação contabilística e fiscal** - artigo 121.º. do CIRC


FAQ Contabilidade
--------------------------------------

### Q: presidente / Tesoureiro de uma associação pode ser o seu [TOC](#tecnico-oficial-de-contas)?
A: Não. Ver
[aqui](https://web.archive.org/web/20190626192425/http://contabilistas.info/index.php?topic=7363.0).

### Q: É necessário a associação passar faturas?
A: Não. As quotizações ao abrigo do nº 20 do artº 29, não é
obrigatorio emitir faturas, podendo ser cumprido a obrigação de emitir
um documento de quitação atraves de outros documentos. O mais comum, o
recibo. ([fonte](http://contabilistas.info/index.php?topic=17734.0)

### Q: A associação paga IRC / IVA?
A: Não, para praticamente tudo. De acordo com a alínea b) e c) do nº1
do artigo 10º do CIRC, podem beneficiar da isenção de IRC “as
instituições particulares de solidariedade social, bem como as pessoas
coletivas àquelas legalmente equiparadas”

> **artigo 9º** As associações estão isentas de IVA se o volume de
> negócios anual for **inferior a 10.000€** (TODO verificar)

### Q: Cuidados devemos ter com transações com dinheiro
A: Transações com dinheiro vivo nunca devem exceder 1000€ quer para
receber como para enviar. 

### Q: Como é que o tesoureiro articula as suas contas com o/a contabilista?
A: Não se articula. O tesoureiro produz os seus próprios dados de
contas e envia-os periodicamente para o contabilista que depois faz a
sua gestão própria.

### Q: Quando podem as finanças inspecionar?
A: podem inspecionar em qualquer altura
- vão encontrar sempre alguma gralha (é humanamente impossivel não
  fazer erros) - o importante é minimizá-los
- convem as contas estarem sempre em dia

::: tip DICA

É recomendado ter sempre todos os documentos impressos para caso haja
uma inspeção pois pode calhar um(a) inspetor(a) que nao saiba bem mexer
num computador.

Organizar uma "pasta" por cada mês com os seguintes registos:
  - **todas** as faturas/recibos emitidos
  - pagamentos bancários (com o **papel da transferência**)

:::

### Q: Queremos cobrar por eventos, como fazer?
ver [Prestação de serviços](#prestacao-de-servicos-alinhado-com-missao)
