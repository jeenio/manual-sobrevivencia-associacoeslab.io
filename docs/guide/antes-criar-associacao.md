# Antes de criar uma associação

::: warning AVISO

Este manual ainda está em desenvolvimento e como tal, não recomendamos
ainda o seu uso. Caso deseje ajudar, veja como
[contribuir](contribuir.md).

:::

Requisitos recomendados
-----------------------

A lista que se segue enumera os requisitos recomendados para
prosseguir com a criação de uma associação: (TODO confirmar com outros)

### 12+ pessoas

  - motivadas
  - acreditam na causa da associação
  - comfiam umas nas outras
  - com NIF (Número de Identificação Fiscal)
  - candidatas(os) à direção dispostas a investir 15-20h nas primeras
    semanas

::: details sobre mínimo de pessoas

O mínimo dos mínimos é ter 6 pessoas quando a associação não tem [Mesa
da Assembleia](glossario.md#mesa-da-assembleia) nomeada ou 9 quando
tem (se criar por [associação na
hora](criar-associacao.md#via-associacao-na-hora) o mínimo é este).

Contudo, recomendamos não ter um número de membros tão baixo pois nem
todos podem querer ser [órgãos
estatutários](glossario.md#orgaos-estatutarios) ou alguém pode decidir
abandonar a associação ou desentender-se.

:::

### Dinheiro

600 € iniciais

  - aprox. 300€ para criação da associação
  - aprox. 300 € para margem de manobra
  - quotas aumentarão este valor


### Morada para a sede

Morada para sede. Apartados (morada virtual nos correios) não são válidos.

::: warning AVISO

:gun: **fuzilamento legal**: Mudar a sede tem um [custo muito elevado](outras-burocracias.md#alterar-a-morada-da-sede) (min. 275€).

Portanto, escolham bem a morada. E tenham a consciência de que será
publicada online.

:::


Quanto trabalho devo esperar?
-----------------------------

Deve prepara-se para o impacto inicial. O sistema português muito
dificulta a criação e manutenção de uma associação, mas isso não a(o)
deve desincentivar a prosseguir a(o) seu fim. Deve dar uma vista de
olhos no manual para ter uma sensção do trabalho que implica.

Mas sensívelmente, deve assumir que para os membros da direção, isto
será como um part-time job que lhe consumirá certa de 5h/semana (TODO:
confirmar com outras associações) mas entre 15-20h nas primeiras
semanas.
