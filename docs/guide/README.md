# Introdução 

::: warning AVISO

Este manual ainda está em desenvolvimento e como tal, não recomendamos
ainda o seu uso. Caso deseje ajudar, veja como
[contribuir](contribuir.md).

:::


-----

Criar uma Associação sem fins lucrativos em Portugal (um tipo de
[ONG](https://gulbenkian.pt/noticias/as-ong-em-portugal/)) não é
trabalho fácil, principalmente começando com poucos fundos e poucos
associados. Este guia está aqui para ajudar.

Este guia de licença aberta e colaborativo permite suavizar o trabalho
burocrático para que a sua associação consiga forcar-se na missão para
a qual foi criada. O objetivo é agregar as melhores práticas de
criação e gestão de associações sem fins lucrativos em Portugal e
idealmente terá contribuições de um variado número de organizações.

Para começar, basta clicar num tópio na barra da esquerda. Para quem
ainda não criou a associação e quer fazê-lo ou saber os requisitos, deve
começar em "[Antes de criar uma associação](antes-criar-ong.md)" e ler por ordem
sequencial as páginas.

Para quem já tenha a sua associação e queira apenas consultar uma dada
secção, pode usar a barra de pesquisa no todo desta página para
encontrar o que procura.

Caso queria entrar [em contacto connosco](TODO) ou leia
sobre como pode [contribuir diretamente](contribute.md) para o manual.

Todo o conteúdo deste manual (excepto se mencionado em contrário) está
disponível sobre a licença aberta [Creative Commons
Attribution-ShareAlike 4.0 International License (CC BY-SA
4.0)](https://creativecommons.org/licenses/by-sa/4.0/). (TODO,
explicar o que é)

Muitos agradecimentos a todos os que tornaram este manual
possível. Para uma lista completa de contribuidores e revisores, veja
[aqui](contribute.md#list-of-contributors).


Principais perigos
------------------

Dado que isto é um manual de sobrevivência, convem enumerar as
principais categorias riscos.

::: warning PERIGO

Todos os riscos estarão bem assinalados em caixas amarelas como esta
ao longo de todo o manual. Portanto não há como os ignorar.

:::

* :gun: **fuzilamento legal** - quando o estado usa metralhas
  burocráticas (multas elevadas) para punir pequenas organizações que
  nem têm capacidade para contratar advogados nem contabilidas para
  não as terem de apanhar.
  
* :chart_with_downwards_trend: **dificuldade de obtenção de
  financiamento** - quando a associação não está a conseguir juntar fundos
  suficientes para sustentar a sua causa

* :writing_hand: **ninguém quer fazer a burocracia** - quando criamos
  uma ONG é porque acreditamos em algo, e a burocracia muitas vezes só
  atrapaha. Isto é o risco de a associação não ter membros suficientes
  a lidar essas partes mais chatas.

* :crossed_swords: **disputas internas** - dado que é uma estrutura
  democrática é normal haver disputas internas. Consegue combater-se
  com uma boa cultura interna.



Principais Predadores
---------------------

Quais as várias entidades com que uma assocaição tem que lidar do ponto de vista legal?


* :snake: **Finanças** - possivelmente o maior predador. Através de multas elevadas pelo incumprimento (por vezes por desconhecimento). É preciso ter cuidado. Este predador não brinca.

* :pig2: **Banco** - Os bancos são lentas organizações burocráticas. Esperem muitas dores de cabeça e papelada por preencher. Mas são eles que tomam conta do dinheiro, por isso é bom [escolher bem a raça](apos-criar-associacao.md#criar-conta-bancaria).

* :whale: **Segurança Social** - Por fazer
