<p align="center">
  <a href="https://manual-sobrevivencia-associacoes.gitlab.io" target="_blank">
    <img width="20%" src="https://manual-sobrevivencia-associacoes.gitlab.io/ong-survival-manual.png" alt="logótipo do manual de sobrevivência das Associações em Portugal">
  </a>
</p>

# <center> Manual de Sobrevivência das Associações em Portugal </center>

Este é o código-fonte do projeto "Manual de Sobrevivência das Associações em
Portugal". O projeto em si pode ser encontrado em:
https://manual-sobrevivencia-associacoes.gitlab.io/

Criar uma Associação sem fins lucrativos em Portugal (um tipo de
[ONG](https://gulbenkian.pt/noticias/as-ong-em-portugal/)) não é
trabalho fácil, principalmente começando com poucos fundos e poucos
associados. Este guia está aqui para ajudar.

Este guia de licença aberta e colaborativo permite suavizar o trabalho
burocrático para que a sua Associação consiga forcar-se na missão para
a qual foi criada.

Este guia pretende agregar as melhores práticas de criação e gestão de
associações sem fins lucrativos em Portugal e idealmente terá
contribuições de um variado número de organizações.

O [site respetivo](https://manual-sobrevivencia-associacoes.gitlab.io/) está
sincronizado com o seu código-fonte (este repositório), portanto
qualquer modificação introduzida aparecerá rápidamente (<5 mins) no
site.


## Linguagem simples

Legalês não é bem-vindo. As explicações são simples o suficiente para
que as pessoas das mais diversas áreas consigam entender e transformar
o texto em prática.


## Um projeto colaborativo

Boas práticas merecem ser registada. Se souber como fazer algo melhor
do que está ecrito neste guia, propõe uma modificação. Contributos são
mais que bem-vindos!


## É apenas um manual

Este recurso serve como apoio mínimo à sobrevivência de uma associação
em Portugal. Mas não consegue substituir completamente advogados ou
contabilistas.


# Contribuir

Questões? Acha que está interessada(o) em contribuir? Informe-se como
o pode fazer em:
https://manual-sobrevivencia-associacoes.gitlab.io/guide/contribuir.html


Excepto quando mencionado em contrário, todo o conteúdo neste manual
está sob a licença [Creative Commons Attribution-ShareAlike 4.0
International License (CC BY-SA
4.0)](https://creativecommons.org/licenses/by-sa/4.0/).

Agradecemos a todos os que para este projeto contribuiram para que se
tornasse uma realizade. Esperemos salvar muitas frustrações e horas
que de outro seriam deperdiçadas em aprender como as burocracias
funcionam.
